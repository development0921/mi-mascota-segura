package com.ar.mms.ui.nutricionDelivery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ar.mms.R;
import com.ar.mms.adapter.NutricionDeliveryAdapter;

import github.chenupt.springindicator.SpringIndicator;

public class NutricionDeliveryFragment extends Fragment {
//public class NutricionDeliveryFragment extends FragmentActivity {

    private NutricionDeliveryViewModel mViewModel;

    public static NutricionDeliveryFragment newInstance() {
        return new NutricionDeliveryFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.nutricion_delivery_fragment, container, false);

        SpringIndicator springIndicator = (SpringIndicator) view.findViewById(R.id.indicator);
        // just set viewPager

        ((AppCompatActivity) getActivity()).getSupportActionBar().hide(); // ocultamos el toolbar
        ViewPager viewPager = view.findViewById(R.id.pager);
        NutricionDeliveryAdapter adapter = new NutricionDeliveryAdapter(getFragmentManager(), getActivity());
        viewPager.setAdapter(adapter);

        springIndicator.setViewPager(viewPager);

        return view;
    }

    @Override
    public void onDestroyView() {
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
        super.onDestroyView();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //   mViewModel = ViewModelProviders.of(this).get(NutricionDeliveryViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
}

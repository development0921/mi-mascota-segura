package com.ar.mms.ui.articulos

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ar.mms.R
import com.ar.mms.helpers.FirebaseHelper
import com.ar.mms.interfaces.ProductListener
import com.ar.mms.network.ProductEntry
import com.ar.mms.ui.carrito.ShoppingCartFragment
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_articulos.view.*


class ArticulosFragment : Fragment(), ProductListener {

    private lateinit var productViewModel: ArticulosViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        productViewModel = ViewModelProviders.of(this).get(ArticulosViewModel::class.java)
        val view = inflater.inflate(R.layout.fragment_articulos, container, false)

        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference().child(FirebaseHelper.getBranchProducts())
        var resultListProducts: MutableList<ProductEntry> = mutableListOf()

        view.recycler_view.setHasFixedSize(true)
        view.recycler_view.layoutManager =
            GridLayoutManager(context, 2, RecyclerView.VERTICAL, false)


        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                resultListProducts = mutableListOf()
                val listProducts = dataSnapshot.children
                //   val resultProductsList;
                for (snapshot in listProducts) {
                    Log.d(
                        "DATA ADD",
                        "Value is: ${snapshot.getValue(ProductEntry::class.javaObjectType)}"
                    )
                    resultListProducts.add(snapshot.getValue(ProductEntry::class.javaObjectType)!!)
                }

                Log.d("DATA", "Value is: ${resultListProducts.size}")
                val adapter_ =
                    ProductCardRecyclerViewAdapter(resultListProducts, this@ArticulosFragment)
                view.recycler_view.adapter = adapter_
                adapter_.notifyDataSetChanged()
            }

            override fun onCancelled(error: DatabaseError) { // Failed to read value
                val adapter_ =
                    ProductCardRecyclerViewAdapter(resultListProducts, this@ArticulosFragment)
                view.recycler_view.adapter = adapter_
                adapter_.notifyDataSetChanged()
                Log.w("DATA", "Failed to read value.", error.toException())
            }
        })

        val largePadding = resources.getDimensionPixelSize(R.dimen.shr_product_grid_spacing)
        val smallPadding = resources.getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small)
        view.recycler_view.addItemDecoration(ProductGridItemDecoration(largePadding, smallPadding))
        return view
    }

    var mProductDetailFragment: ProductDetailFragment? = null
    override fun onDetails(productEntry: ProductEntry?) {
        Log.d("onDetails", "Value is: ${ProductEntry::class.javaObjectType}")
        val fm: FragmentManager = requireActivity()?.supportFragmentManager
         mProductDetailFragment = ProductDetailFragment.newInstance(productEntry)
        requireActivity()?.supportFragmentManager.beginTransaction()
            .replace(R.id.nav_host_fragment, mProductDetailFragment!!, "fragment_detail")
            //   .addToBackStack(null)
            .commit()
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.toolbar_menu_cart, menu)
        super.onCreateOptionsMenu(menu, menuInflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.search -> {
                executeSearch();
                return true
            }
            R.id.nav_shopping_cart_id -> {
                showShoppingCart()
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

    private fun executeSearch() {
        view?.let {
            Snackbar.make(it, "executeSearch", Snackbar.LENGTH_SHORT)
                .show()
        };
    }

    private fun showShoppingCart() {
        view?.let {
            Snackbar.make(it, "showShoppingCart", Snackbar.LENGTH_SHORT)
                .show()
        };

        val nextFrag = ShoppingCartFragment()
        activity!!.supportFragmentManager.beginTransaction()
            .replace(R.id.nav_host_fragment, nextFrag, "ShoppingCartFragment")
            .addToBackStack(null)
            .commit()
    }

    override fun onDestroy() {
        super.onDestroy()
        val nextFrag = ShoppingCartFragment()
        mProductDetailFragment?.let {
            requireActivity()!!.supportFragmentManager.beginTransaction()
                .remove(it)
                .addToBackStack(null)
                .commit()
        }
    }

}
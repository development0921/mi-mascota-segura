package com.ar.mms.ui.eventos


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ar.mms.R
import com.ar.mms.model.Events
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_item_events.view.*
import java.lang.IllegalArgumentException

class EventsAdapter(
    private val context: Context,
    private val eventsList: List<Events>,
    private val itemClickListener: OnClickListener
) : RecyclerView.Adapter<EventsAdapter.EventsViewHolder>() {

    override fun getItemCount(): Int = eventsList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsViewHolder {
        return EventsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.row_item_events, parent, false)
        )
    }

    override fun onBindViewHolder(holder: EventsViewHolder, position: Int) {
        when (holder) {
            is EventsViewHolder -> holder.bind(eventsList[position])
            else -> throw IllegalArgumentException("Se olvideo de pasar el Viewholder en el bind")  //fixme esto jamas pasara...
        }
    }

    inner class EventsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Events) {
        //    itemView.setOnClickListener { itemClickListener.onItemClick(item.id) }
         //   itemView.imageView.setOnClickListener { itemClickListener.onImageClick() }
            Glide.with(context).load(item.imagen).into(itemView.img_events_id)
            itemView.text_events.text = item.name
            itemView.description_id.text = item.descripcion
            itemView.direccion_id.text = item.direccion
            itemView.fecha_id.text = item.fecha
        }
    }

    interface OnClickListener {
        fun onImageClick(image: String)
        fun onItemClick(id: Long)
    }

}
package com.ar.mms.ui.contacto

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.ar.mms.R
import com.ar.mms.databinding.FragmentContactoBinding

class ContactoFragment : Fragment(), View.OnClickListener {

    private var _binding: FragmentContactoBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentContactoBinding.inflate(inflater, container, false)
        setUp()
        return binding.root
    }

    private fun setUp() {
        binding.btnWhatsapp.setOnClickListener(this)
        binding.btnFacebook.setOnClickListener(this)
        binding.btnInstagram.setOnClickListener(this)
        binding.btnMail.setOnClickListener(this)
        binding.btnWeb.setOnClickListener(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onClick(v: View?) {
       // Toast.makeText(activity, "${v?.id}", Toast.LENGTH_SHORT).show()
        when(v?.id){
            R.id.btnWhatsapp ->{goToUrl("https://wa.me/qr/DT2BAL55C3ETE1")}
            R.id.btnFacebook ->{goToUrl("https://www.facebook.com/Mascotaseguraar-679958685761599")}
            R.id.btnInstagram ->{goToUrl("https://www.instagram.com/mascotasegura.ar?r=nametag")}
            R.id.btnMail ->{}
            R.id.btnWeb ->{goToUrl("http://www.mimascotasegura.com/")}
        }
    }

    private fun goToUrl(url:String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

}
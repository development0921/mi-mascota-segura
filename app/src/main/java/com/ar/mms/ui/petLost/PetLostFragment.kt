package com.ar.mms.ui.petLost

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ar.mms.R
import com.ar.mms.model.PetLost
import com.google.firebase.database.FirebaseDatabase
import java.util.HashMap

class PetLostFragment : Fragment(), PetLostAdapter.OnClickListener {

    private lateinit var recyclerView: RecyclerView
    private val mDatabase = FirebaseDatabase.getInstance().reference
    private var mAdoptionList = mutableListOf<PetLost>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_pet_lost, container, false)
        setUpView(view)
        return view
    }

    private fun setUpView(view: View) {
        mAdoptionList = emptyList<PetLost>().toMutableList()
        recyclerView = view.findViewById(R.id.recycler_adoption)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {

        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                activity,
                DividerItemDecoration.VERTICAL
            )
        )

        getVeterinariasFromFirebaseAndUpdateRecyclerView()

    }

    fun getVeterinariasFromFirebaseAndUpdateRecyclerView() {

        mDatabase.child("lost").get().addOnSuccessListener { dataSnapshot ->
            Log.i("firebase adoption&events", "Got value ${dataSnapshot.value}")

            for (postSnapshot in dataSnapshot.children) {
                val adoptionFirebase = (postSnapshot.value) as HashMap<String, String>
                val adoption = PetLost(
                    id = adoptionFirebase["id"] as Long,
                    name = adoptionFirebase["name"].toString(),
                    imagen = adoptionFirebase["imagen"].toString(),
                    contacto = adoptionFirebase["contacto"].toString(),
                    direccion = adoptionFirebase["direccion"].toString(),
                    fecha = adoptionFirebase["fecha"].toString(),
                    //   description = adoptionFirebase["description"].toString()
                )
                mAdoptionList.add(adoption)
            }

            showRecyclerView()

        }.addOnFailureListener {
            Log.e("firebase adoption", "Error getting data", it)
        }
    }

    private fun showRecyclerView() {
        if (mAdoptionList.isNotEmpty())
            recyclerView.adapter = activity?.applicationContext?.let {
                PetLostAdapter(it, mAdoptionList, this)
            }
    }


    override fun onImageClick(image: String) {
        TODO("Not yet implemented")
    }

    override fun onItemClick(id: Long) {
         val action = PetLostFragmentDirections.actionNavPetLostIdToPetLostDetailsId(mAdoptionList.first { it.id == id })
         findNavController()?.navigate(action)
    }
}



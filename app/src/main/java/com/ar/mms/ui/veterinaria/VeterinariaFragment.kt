package com.ar.mms.ui.veterinaria

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ar.mms.R
import com.ar.mms.model.Veterinaria
import com.google.firebase.database.FirebaseDatabase
import java.util.HashMap


class VeterinariaFragment : Fragment(), VeterinariaAdapter.OnClickListener {

    private lateinit var recyclerView: RecyclerView

    private val mDatabase = FirebaseDatabase.getInstance().reference

    private var mVeterinarias = mutableListOf<Veterinaria>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_veterinaria, container, false)
        setUpView(view)
        return view
    }

    private fun setUpView(view:View) {
        recyclerView = view.findViewById(R.id.recycler_veterinarias)
        setupRecyclerView()
    }


    private fun setupRecyclerView() {

        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                activity,
                DividerItemDecoration.VERTICAL
            )
        )

        getVeterinariasFromFirebaseAndUpdateRecyclerView()

    }

    fun getVeterinariasFromFirebaseAndUpdateRecyclerView() {

        mDatabase.child("vet").get().addOnSuccessListener { dataSnapshot ->
            Log.i("firebase veterinaria", "Got value ${dataSnapshot.value}")


            for (postSnapshot in dataSnapshot.children) {
                val veterinariaFirebase = (postSnapshot.value) as HashMap<String, String>
                val veterinaria = Veterinaria(
                    id = veterinariaFirebase["id"] as Long,
                    name = veterinariaFirebase["name"].toString(),
                    imagen = veterinariaFirebase["imagen"].toString(),
                    contacto = veterinariaFirebase["contacto"].toString(),
                    direccion = veterinariaFirebase["direccion"].toString(),
                    gps = veterinariaFirebase["longitude&latitude"].toString(),
                    description = veterinariaFirebase["description"].toString()

                )
                mVeterinarias.add(veterinaria)
            }

            showRecyclerView()

        }.addOnFailureListener {
            Log.e("firebase veterinaria", "Error getting data", it)
        }

    }

    private fun showRecyclerView() {
        if (mVeterinarias.isNotEmpty())
            recyclerView.adapter = activity?.applicationContext?.let {
                VeterinariaAdapter(it, mVeterinarias, this)
            }
    }


    override fun onImageClick(image: String) {
        TODO("Not yet implemented")
    }

    override fun onItemClick(id: Long) {
        val action = VeterinariaFragmentDirections.actionNavVeterinariaIdToVeterinasDetailsActivity(
            mVeterinarias.first { it.id == id }
        )
        findNavController()?.navigate(action)
    }


}



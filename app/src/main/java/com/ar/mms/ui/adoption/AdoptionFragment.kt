package com.ar.mms.ui.adoption

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.util.Log
import android.view.View
import com.ar.mms.R
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ar.mms.model.Adoption
import com.google.firebase.database.FirebaseDatabase
import java.util.HashMap
import android.net.Uri

/**
 * A simple [Fragment] subclass.
 */
class AdoptionFragment : Fragment(), AdoptionAdapter.OnClickListener {

    private lateinit var recyclerView: RecyclerView
    private val mDatabase = FirebaseDatabase.getInstance().reference
    private var mAdoptionList = mutableListOf<Adoption>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_adoption, container, false)
        setUpView(view)
        return view
    }

    private fun setUpView(view: View) {
        mAdoptionList = emptyList<Adoption>().toMutableList()
        recyclerView = view.findViewById(R.id.recycler_adoption)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {

        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                activity,
                DividerItemDecoration.VERTICAL
            )
        )

        getVeterinariasFromFirebaseAndUpdateRecyclerView()

    }

    fun getVeterinariasFromFirebaseAndUpdateRecyclerView() {

        mDatabase.child("adoption").get().addOnSuccessListener { dataSnapshot ->
            Log.i("firebase adoption&events", "Got value ${dataSnapshot.value}")

            for (postSnapshot in dataSnapshot.children) {
                val adoptionFirebase = (postSnapshot.value) as HashMap<String, String>
                val adoption = Adoption(
                    id = adoptionFirebase["id"] as Long,
                    name = adoptionFirebase["name"].toString(),
                    imagen = adoptionFirebase["imagen"].toString(),
                    contacto = adoptionFirebase["contacto"].toString(),
                    direccion = adoptionFirebase["direccion"].toString(),
                    fecha = adoptionFirebase["fecha"].toString(),
                    description = adoptionFirebase["description"].toString()
                )
                mAdoptionList.add(adoption)
            }

            showRecyclerView()

        }.addOnFailureListener {
            Log.e("firebase adoption", "Error getting data", it)
        }
    }

    private fun showRecyclerView() {
        if (mAdoptionList.isNotEmpty())
            recyclerView.adapter = activity?.applicationContext?.let {
                AdoptionAdapter(it, mAdoptionList, this)
            }
    }

    override fun onImageClick(image: String) {
        TODO("Not yet implemented")
    }

    override fun onItemClick(id: Long) {
        val action = AdoptionFragmentDirections.actionNavAdoptionIdToAdoptionDetailsId(mAdoptionList.first { it.id == id })
        findNavController()?.navigate(action)
    }

//    override fun onClick(v: View?) {
        // Toast.makeText(activity, "${v?.id}", Toast.LENGTH_SHORT).show()
//        when(v?.id){
//            R.id.linkTextshare2 ->{goToUrl("https://wa.me/qr/DT2BAL55C3ETE1")}
//        }
//    }

//    private fun goToUrl(url:String) {
//        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
//    }


}


//val donaciones = """ Jesus Anibal Mora Rosales.
//                     Cuit/cuil: 20-95894664-4
//                     Banco: Santander
//                     Tipo de cuenta: Cuenta única.
//                     Sucursal: 395-Plaza Serrano
//                     Cuenta: 395-359362/8
//                     CBU: 0720395288000035936286
//                     Alias: MMS.ANIBAL"""
package com.ar.mms.ui.saludEnLinea;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.ar.mms.R;


public class SaludEnLineaFragment extends Fragment {

    private SaludEnLineaViewModel mSaludEnLinea;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mSaludEnLinea = ViewModelProviders.of(this).get(SaludEnLineaViewModel.class);
        View root = inflater.inflate(R.layout.fragment_salud_en_linea, container, false);
        final TextView textView = root.findViewById(R.id.text_share);
        mSaludEnLinea.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}
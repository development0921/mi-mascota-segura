package com.ar.mms.ui.adoption

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ar.mms.databinding.FragmentEventsDetailBinding
import com.ar.mms.model.Adoption
import com.bumptech.glide.Glide

class AdoptionDetailsFragment : Fragment() {


    private lateinit var binding: FragmentEventsDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentEventsDetailBinding.inflate(inflater)

        if (arguments != null) {
            val adoption: Adoption? = AdoptionDetailsFragmentArgs.fromBundle(requireArguments()).adoptionSelectedProperty

            binding.tvNameAdoption.text = adoption?.name
            binding.descriptionId.text = adoption?.description
            binding.direccionId.text = adoption?.direccion
            binding.contactoLost.text = adoption?.contacto


            Glide.with(this).load(adoption?.imagen).into(binding.imgAdoptionId)
        }

        return binding.root
    }

}
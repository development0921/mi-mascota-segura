package com.ar.mms.ui.eventos

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ar.mms.databinding.FragmentEventsDetailBinding
import com.ar.mms.model.Events

class EventsDetailFragment : Fragment() {


    private lateinit var binding: FragmentEventsDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentEventsDetailBinding.inflate(inflater)

        if (arguments != null) {
            val eventos: Events? =
                EventsDetailFragmentArgs.fromBundle(requireArguments()).eventsSelectedProperty

//            binding.name.text = veterinarias?.name
//            binding.description.text = veterinarias?.description
//
//            Glide.with(this).load(veterinarias?.image).into(binding.photoView)
        }

        return binding.root
    }
}


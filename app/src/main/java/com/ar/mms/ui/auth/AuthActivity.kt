package com.ar.mms.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.ar.mms.R
import com.ar.mms.activities.MainActivity
import com.ar.mms.databinding.ActivityAuthBinding
import com.ar.mms.helpers.MmsSharedPreferences
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_auth.*

enum class ProviderType{
    GOOGLE
}

class AuthActivity : AppCompatActivity() {

    private val GOOGLE_SIGN_IN = 100
    private lateinit var binding: ActivityAuthBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityAuthBinding.inflate(layoutInflater)

        //binding.editTextEmail.setText("johan@gmail.com")
        //binding.editTextPass.setText("1234")
        setContentView(binding.root)

        setUpRegisterFirebase()
        setUpLoginFirebase()

        retrieveSession()

    }

    private fun retrieveSession() {
        if (FirebaseAuth.getInstance().currentUser != null ||  MmsSharedPreferences.instance?.inSession == true){
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    private fun setUpRegisterFirebase() {

        binding.btnRegister.setOnClickListener {
            var mail = binding.editTextEmail.text.toString()
            var pass = binding.editTextPass.text.toString()

            if (mail.isNotEmpty() && pass.isNotEmpty()) {
                FirebaseAuth.getInstance()
                    .createUserWithEmailAndPassword(mail, pass)
                    .addOnCompleteListener { result ->
                        if (result.isSuccessful) {
                            showAlert("Registro completado")
                            MmsSharedPreferences.instance?.saveSession(true)
                            startActivity(Intent(this, MainActivity::class.java))
                            finish()
                        } else {
                            showAlert("Error al registrar, usuario ya existente")
                        }
                    }
            } else {
                showAlert("Ingrese usuario y password")
            }
        }
    }

    //fixme: jv: validar pass al momento de llamar...
    private fun setUpLoginFirebase() {

        binding.btnLogin.setOnClickListener {
            var mail = binding.editTextEmail.text.toString()
            var pass = binding.editTextPass.text.toString()

            if (mail.isNotEmpty() && pass.isNotEmpty()) {
                FirebaseAuth.getInstance()
                    .signInWithEmailAndPassword(mail, pass)
                    .addOnCompleteListener { result ->
                        if (result.isSuccessful) {
                            MmsSharedPreferences.instance?.saveSession(true)
                            startActivity(Intent(this, MainActivity::class.java))
                            finish()
                        } else {
                            showAlert("Error al inicio de sesion, usuario invalido")
                        }
                    }
            } else {
                showAlert("Ingrese usuario y password")
            }
        }
        googleButton.setOnClickListener {

            //configuracion
            val googleConf = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
            val googleClient = GoogleSignIn.getClient(this, googleConf)
            googleClient.signOut()

            startActivityForResult(googleClient.signInIntent, GOOGLE_SIGN_IN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == GOOGLE_SIGN_IN){

            val tasks = GoogleSignIn.getSignedInAccountFromIntent(data)

            try {
                val account = tasks.getResult(ApiException::class.java)

                if(account != null){

                    val credentials = GoogleAuthProvider.getCredential(account.idToken, null)

                    FirebaseAuth.getInstance().signInWithCredential(credentials).addOnCompleteListener {

                        if (it.isSuccessful) {
                            MmsSharedPreferences.instance?.saveSession(true)
                            startActivity(Intent(this, MainActivity::class.java))
                            finish()
                        } else {
                            showAlert("Error al inicio de sesion, usuario invalido")
                        }
                    }

                }

            } catch (e: ApiException) {
                showAlert(e.localizedMessage)
            }

        }
    }


    private fun showAlert(msg: String) {

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("$msg")
        builder.setPositiveButton("Aceptar", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}
package com.ar.mms.ui.petLost

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ar.mms.R
import com.ar.mms.model.PetLost
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_item_adoption.view.*
import java.lang.IllegalArgumentException


class PetLostAdapter(
    private val context: Context,
    private val eventsList: List<PetLost>,
    private val itemClickListener: OnClickListener
) : RecyclerView.Adapter<PetLostAdapter.PetLostViewHolder>() {

    override fun getItemCount(): Int = eventsList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PetLostViewHolder {
        return PetLostViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.row_item_adoption, parent, false)
        )
    }

    override fun onBindViewHolder(holder: PetLostViewHolder, position: Int) {
        when (holder) {
            is PetLostViewHolder -> holder.bind(eventsList[position])
            else -> throw IllegalArgumentException("Se olvideo de pasar el Viewholder en el bind")  //fixme esto jamas pasara...
        }
    }

    inner class PetLostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: PetLost) {
            itemView.setOnClickListener { itemClickListener.onItemClick(item.id) }
            //  itemView.img_adoption_id.setOnClickListener { itemClickListener.onImageClick() }
            Glide.with(context).load(item.imagen).into(itemView.img_adoption_id)
            itemView.tv_name_adoption.text = item.name
            itemView.tv_descripcion_id.text = item.direccion
        }
    }

    interface OnClickListener {
        fun onImageClick(image: String)
        fun onItemClick(id: Long)
    }

}
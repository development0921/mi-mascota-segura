package com.ar.mms.ui.eventos

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ar.mms.R
import com.ar.mms.model.Events
import com.google.firebase.database.FirebaseDatabase
import java.util.HashMap

class EventosFragment :  Fragment(), EventsAdapter.OnClickListener {

    private lateinit var recyclerView: RecyclerView
    private val mDatabase = FirebaseDatabase.getInstance().reference
    private var mEvents = mutableListOf<Events>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_eventos, container, false)
        setUpView(view)
        return view
    }

    private fun setUpView(view:View) {
        mEvents = emptyList<Events>().toMutableList()
        recyclerView = view.findViewById(R.id.recycler_events_id)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {

        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                activity,
                DividerItemDecoration.VERTICAL
            )
        )

        getVeterinariasFromFirebaseAndUpdateRecyclerView()

    }

    fun getVeterinariasFromFirebaseAndUpdateRecyclerView() {


        mDatabase.child("raffles&events").get().addOnSuccessListener { dataSnapshot ->
            Log.i("firebase raffles&events", "Got value ${dataSnapshot.value}")


            for (postSnapshot in dataSnapshot.children) {
                val eventsFirebase = (postSnapshot.value) as HashMap<String, String>
                val events = Events(
                    id = eventsFirebase["id"] as Long,
                    name = eventsFirebase["name"].toString(),
                    imagen = eventsFirebase["imagen"].toString(),
                    contacto = eventsFirebase["contacto"].toString(),
                    descripcion = eventsFirebase["descripcion"].toString(),
                    direccion = eventsFirebase["direccion"].toString(),
                    fecha = eventsFirebase["fecha"].toString()
                )
                mEvents.add(events)
            }

            showRecyclerView()

        }.addOnFailureListener {
            Log.e("firebase raffles", "Error getting data", it)
        }
    }

    private fun showRecyclerView() {
        if (mEvents.isNotEmpty())
            recyclerView.adapter = activity?.applicationContext?.let {
                EventsAdapter(it, mEvents, this)
            }
    }


    override fun onImageClick(image: String) {
        TODO("Not yet implemented")
    }

    override fun onItemClick(id: Long) {
//        val action = EventosFragmentDirections.actionNavEventsIdToEventsDetailsId(mEvents.first { it.id == id })
//        findNavController()?.navigate(action)
    }

}



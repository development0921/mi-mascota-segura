package com.ar.mms.ui.petLost

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ar.mms.databinding.FragmentPetLostBinding
import com.ar.mms.databinding.FragmentPetLostDetailsBinding
import com.ar.mms.model.Adoption
import com.ar.mms.model.PetLost
import com.bumptech.glide.Glide


class PetLostDetailsFragment : Fragment() {

    private lateinit var binding: FragmentPetLostDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentPetLostDetailsBinding.inflate(inflater)


        if (arguments != null) {
            val petLost: PetLost? =  PetLostDetailsFragmentArgs.fromBundle(requireArguments()).petLostSelectedProperty

            binding.tvNameAdoption.text = petLost?.name
         //  binding.descriptionId.text = petLost?.description
            binding.direccionId.text = petLost?.direccion
            binding.contactoLost.text = petLost?.contacto


            Glide.with(this).load(petLost?.imagen).into(binding.imgAdoptionId)
        }

        return binding.root
    }

}




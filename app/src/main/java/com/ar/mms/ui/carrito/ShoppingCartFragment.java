package com.ar.mms.ui.carrito;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.ar.mms.R;
import com.ar.mms.room.viewmodel.ShoppingBasketViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShoppingCartFragment extends Fragment {

    @BindView(R.id.act_delivery_control_recycler)
    RecyclerView mRecyclerView;
    @BindView(R.id.act_delivery_control_emptyState)
    TextView mEmptyState;
    @BindView(R.id.act_delivery_control_progressBar)
    ProgressBar progressBar;

    private ShoppingBasketViewModel mValidationViewModel;
    private  SoppingAdapter mAdapterDeliveryControl;
    private LinearLayoutManager mLinearLayout ;
    private Integer limitPaginationInt = 25;
    private Integer offsetPaginationInt = 0;
    private Integer loadMoreItemsNumber = 5;
    private Context mContext;

    public static ShoppingCartFragment newInstance() {
        ShoppingCartFragment fragment = new ShoppingCartFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mValidationViewModel = new ViewModelProvider(ShoppingCartFragment.this).get(ShoppingBasketViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.shopping_control, container, false);
        ButterKnife.bind(this, v);
        mLinearLayout = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLinearLayout);
        mAdapterDeliveryControl = new SoppingAdapter(mContext);
        mRecyclerView.setAdapter(mAdapterDeliveryControl);
        mRecyclerView.setHasFixedSize(true);

        mValidationViewModel.getValidationPagination(25, 0).observe(getViewLifecycleOwner(), validations -> {
            mAdapterDeliveryControl.setListaOrders(validations);
            if (validations.size() != 0) {
                mRecyclerView.setVisibility(View.VISIBLE);
                mEmptyState.setVisibility(View.GONE);
            } else {
                mRecyclerView.setVisibility(View.GONE);
                mEmptyState.setVisibility(View.VISIBLE);
            }
            mAdapterDeliveryControl.notifyDataSetChanged();
        });
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int recyclerViewUltimaPosicionVisible = mLinearLayout.findLastVisibleItemPosition();
                int totalElementosRecyclerView = mLinearLayout.getItemCount();
                if (recyclerViewUltimaPosicionVisible >= totalElementosRecyclerView - loadMoreItemsNumber & offsetPaginationInt < totalElementosRecyclerView) {
                    progressBar.setVisibility(View.VISIBLE);
                    offsetPaginationInt = offsetPaginationInt + limitPaginationInt;
                    mValidationViewModel.getValidationPagination(limitPaginationInt, offsetPaginationInt).observe(getViewLifecycleOwner(), validations -> {
                        progressBar.setVisibility(View.GONE);
                        mAdapterDeliveryControl.setMorePages(validations);
                    });
                }
            }
        });

        return v;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }


}

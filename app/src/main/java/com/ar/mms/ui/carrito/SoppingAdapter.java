package com.ar.mms.ui.carrito;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.ar.mms.R;
import com.ar.mms.room.entity.ShoppingBasket;

import java.util.List;

public class SoppingAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private List<ShoppingBasket> mDeliveriesList;

    public SoppingAdapter(Context context) {
        this.mContext = context;
        notifyDataSetChanged();
    }

    public SoppingAdapter(Context context, List<ShoppingBasket> mDeliveriesList) {
        this.mContext = context;
        this.mDeliveriesList = mDeliveriesList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View viewCeldaOther = layoutInflater.inflate(R.layout.adapter_control_item, parent, false);
        RecyclerView.ViewHolder viewHolder = new ViewHolderDeliveryControl(viewCeldaOther);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ShoppingBasket delivery = mDeliveriesList.get(position);

        SoppingAdapter.ViewHolderDeliveryControl messageReceiveViewHolder = (SoppingAdapter.ViewHolderDeliveryControl) holder;
        messageReceiveViewHolder.loadHistory(delivery);
    }

    @Override
    public int getItemCount() {
        return (mDeliveriesList == null) ? 0 : mDeliveriesList.size();
    }

    public void setListaOrders(List<ShoppingBasket> mDeliveriesList) {
        this.mDeliveriesList = mDeliveriesList;
    }

    public void setMorePages(List<ShoppingBasket> validationList) {
        this.mDeliveriesList.addAll(validationList);
        notifyDataSetChanged();
    }


    private class ViewHolderDeliveryControl extends RecyclerView.ViewHolder {

        private TextView mDeliveryBag;
        private TextView mDeliveryQuantity;
        private TextView mDeliveryDate;
        private TextView mDocument;
        private TextView mDocumentType;
        private TextView mName;
        private TextView mLastName;

        public ViewHolderDeliveryControl(@NonNull View itemView) {
            super(itemView);

            mDeliveryBag = itemView.findViewById(R.id.delivery_bag);
            mDeliveryQuantity = itemView.findViewById(R.id.delivery_quantity);
            mDeliveryDate = itemView.findViewById(R.id.delivery_date);
            mDocumentType = itemView.findViewById(R.id.delivery_document_type);
            mDocument = itemView.findViewById(R.id.delivery_document_number);
            mName = itemView.findViewById(R.id.delivery_name);
            mLastName = itemView.findViewById(R.id.delivery_lastname);
        }

        private void loadHistory(final ShoppingBasket validation) {

            mDeliveryBag.setText(validation.getName());
            mDeliveryQuantity.setText(String.valueOf(validation.getQuantity()));
            mDeliveryDate.setText(validation.getDate());
            mDocumentType.setText(String.valueOf(validation.getAmount()));
//            mDocument.setText(validation.getDocumentNumber());
//            mName.setText(validation.getFullName());
//            mLastName.setText(validation.getLastName());
        }
    }

}

package com.ar.mms.ui.articulos

import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.NetworkImageView

import com.ar.mms.R

class ProductCardViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var productContainer: LinearLayout = itemView.findViewById(R.id.product_container)
    var productImage: NetworkImageView = itemView.findViewById(R.id.product_image)
    var productTitle: TextView = itemView.findViewById(R.id.product_title)
    var productPrice: TextView = itemView.findViewById(R.id.product_price)
}
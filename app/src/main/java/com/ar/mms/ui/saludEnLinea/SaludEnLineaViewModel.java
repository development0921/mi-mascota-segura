package com.ar.mms.ui.saludEnLinea;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SaludEnLineaViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public SaludEnLineaViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("RECOMENDACION: Luego de ser atendido por uno de nuestros especialistas, acerquense a una veterinaria afiliada y disfruta de los descuentos");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
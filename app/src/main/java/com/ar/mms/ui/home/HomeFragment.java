package com.ar.mms.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.ar.mms.R;
import com.ar.mms.activities.DatosMascotasActivity;
import com.ar.mms.model.Owner;
import com.ar.mms.model.Pet;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private HomeViewModel homeViewModel;
    private EditText mDatosMascota;
    private String TAG = HomeFragment.class.getSimpleName();
    private  ImageView mBtnBuscar;

    private RelativeLayout mProgressBar;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        init(view);

        mProgressBar = view.findViewById(R.id.loading_indicator);
        mProgressBar.setVisibility(View.GONE);

        return view;
    }

    private void init(View view) {
        mDatosMascota = view.findViewById(R.id.btn_buscar_mascota_por_id);
        mBtnBuscar = view.findViewById(R.id.btn_buscar_id);
        mBtnBuscar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_buscar_id:

                String id = mDatosMascota.getText().toString();
                if (!id.isEmpty()) {
                    mBtnBuscar.setEnabled(false);
                    mProgressBar.setVisibility(View.VISIBLE);
                    onResponseFirebaseForPetById(id);
                } else {
                    Toast.makeText(getActivity(), "Para continuar introduzca el número de la mascota", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    public void onResponseFirebaseForPetById(String id) {
        mDatabase.child("pets").child(id).get()
                .addOnFailureListener(it -> {
                    mBtnBuscar.setEnabled(true);
                    mProgressBar.setVisibility(View.GONE);
                })
                .addOnCompleteListener(task -> {
                    mBtnBuscar.setEnabled(true);
                    mProgressBar.setVisibility(View.GONE);
                    Log.d("firebase", String.valueOf(task.getResult().getValue()));

                    if (task.getResult().getValue() == null) {
                        Toast.makeText(getActivity(), "Mascota no registrada", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (task.getResult().getValue() instanceof HashMap) {

                        HashMap<String, String> petFirebase = (HashMap<String, String>) task.getResult().getValue();

                        Pet petInfo = new Pet(
                                petFirebase.get("avatar"),
                                petFirebase.get("birth"),
                                petFirebase.get("clinicHistory"),
                                String.valueOf(petFirebase.get("code")),
                                petFirebase.get("gender"),
                                petFirebase.get("lastCheck"),
                                petFirebase.get("name"),
                                String.valueOf(petFirebase.get("owner")),
                                petFirebase.get("race"),
                                petFirebase.get("type"),
                                petFirebase.get("weight")
                        );

                        onResponseFirebaseForOwnerById(id, petInfo);

                    }
                });
    }

    public void onResponseFirebaseForOwnerById(String id, Pet petInfo) {
        mDatabase.child("owners").child(id).get().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                mProgressBar.setVisibility(View.GONE);
                Log.e("firebase", "Error getting data", task.getException());
            } else {
                Log.d("firebase", String.valueOf(task.getResult().getValue()));

                if (task.getResult().getValue() instanceof HashMap) {

                    HashMap<String, String> ownerFirebase = (HashMap<String, String>) task.getResult().getValue();

                    Owner ownerInfo = new Owner(
                            ownerFirebase.get("DireccionVeteAfiliada"),
                            ownerFirebase.get("TelefonoVeteAfeiliada"),
                            ownerFirebase.get("address"),
                            ownerFirebase.get("birthday"),
                            ownerFirebase.get("city"),
                            ownerFirebase.get("country"),
                            ownerFirebase.get("dni"),
                            ownerFirebase.get("email"),
                            ownerFirebase.get("firstName"),
                            String.valueOf(ownerFirebase.get("idPets")),
                            ownerFirebase.get("lastName"),
                            ownerFirebase.get("manager"),
                            ownerFirebase.get("nacionality"),
                            String.valueOf(ownerFirebase.get("petQty")),
                            ownerFirebase.get("phone"),
                            ownerFirebase.get("status"),
                            ownerFirebase.get("user")
                    );

                    startActivity(petInfo, ownerInfo);
                }
            }
        });
    }

    private void startActivity(Pet petInfo, Owner ownerInfo) {
        Intent intent = new Intent(getActivity(), DatosMascotasActivity.class);
        intent.putExtra("petInfo", petInfo);
        intent.putExtra("ownerInfo", ownerInfo);
        startActivity(intent);
        mProgressBar.setVisibility(View.GONE);

    }

}



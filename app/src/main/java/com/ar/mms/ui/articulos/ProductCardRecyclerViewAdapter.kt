package com.ar.mms.ui.articulos

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ar.mms.R
import com.ar.mms.interfaces.ProductListener
import com.ar.mms.network.ImageRequester
import com.ar.mms.network.ProductEntry


/**
 * Adapter used to show a simple grid of products.
 */
class ProductCardRecyclerViewAdapter internal constructor(
    private val productList: List<ProductEntry>?,
    private val mListener: ProductListener
) :
    RecyclerView.Adapter<ProductCardViewHolder>() {
    private val imageRequester: ImageRequester
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductCardViewHolder {
        Log.d("DATA", "ProductCardRecyclerViewAdapter is: ${productList.toString()}")

        val layoutView =
            LayoutInflater.from(parent.context).inflate(R.layout.product_card, parent, false)
        return ProductCardViewHolder(layoutView)
    }

    override fun onBindViewHolder(holder: ProductCardViewHolder, position: Int) {
        if (productList != null && position < productList.size) {
            val product = productList[position]
            Log.d("DATA ", "ProductCardRecyclerViewAdapter is: ${product.toString()}")

            holder.productTitle.text = product.name
            holder.productPrice.text = product.amount.toString()
            imageRequester.setImageFromUrl(holder.productImage, product.srcimg.toString())

            holder.productContainer.setOnClickListener(View.OnClickListener {

                mListener.onDetails(productList[position])
            })

        }
    }

    override fun getItemCount(): Int {
        return productList!!.size
    }

    init {
        imageRequester = ImageRequester
    }
}


package com.ar.mms.ui.veterinaria

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.navArgs
import com.ar.mms.R
import com.ar.mms.model.Veterinaria
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class VeterinasDetailsActivity : AppCompatActivity(), OnMapReadyCallback {

    //   private lateinit var  binding: ActivityVeterinasDetailsBinding

    private lateinit var mMap: GoogleMap

    val mArgs: VeterinasDetailsActivityArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_veterinas_details)
        //     binding = ActivityVeterinasDetailsBinding.inflate(layoutInflater)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        val veterinaria: Veterinaria? = mArgs.veterinariaSelectedProperty

        if (veterinaria != null) {

            findViewById<TextView>(R.id.name).text = veterinaria.name
            findViewById<TextView>(R.id.description).text = veterinaria.description
            findViewById<TextView>(R.id.contacto).text = veterinaria.contacto
            Glide.with(this).load(veterinaria?.imagen).into(findViewById(R.id.photo_view))
        }

    }
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val gps = mArgs.veterinariaSelectedProperty?.gps?.split(",")

        // Add a marker in Sydney and move the camera
        if (gps != null && gps.isNotEmpty()) {
            val latLng = LatLng(gps[0].trim().toDouble(), gps[1].trim().toDouble())
            mMap.addMarker(MarkerOptions().position(latLng))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f))
        }
    }

}
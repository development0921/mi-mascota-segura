package com.ar.mms.ui.veterinaria

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ar.mms.R
import com.ar.mms.model.Veterinaria
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.veterinarias_row.view.*
import java.lang.IllegalArgumentException


class VeterinariaAdapter(
    private val context: Context,
    private val veterinariaList: List<Veterinaria>,
    private val itemClickListener: OnClickListener
) : RecyclerView.Adapter<VeterinariaAdapter.VeterinariasViewHolder>() {

    override fun getItemCount(): Int = veterinariaList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VeterinariasViewHolder {
        return VeterinariasViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.veterinarias_row, parent, false)
        )
    }

    override fun onBindViewHolder(holder: VeterinariasViewHolder, position: Int) {
        when (holder) {
            is VeterinariasViewHolder -> holder.bind(veterinariaList[position])
            else -> throw IllegalArgumentException("Se olvideo de pasar el Viewholder en el bind")  //fixme esto jamas pasara...
        }
    }

    inner class VeterinariasViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Veterinaria) {
            itemView.setOnClickListener { itemClickListener.onItemClick(item.id) }
            //itemView.img_veterinaria.setOnClickListener { itemClickListener.onImageClick(item.imagen) }
            Glide.with(context).load(item.imagen).into(itemView.img_veterinaria)
            itemView.txt_nombre_veterinaria.text = item.name
            itemView.tv_descripcion_id.text = item.direccion
        }
    }

    interface OnClickListener {
        fun onImageClick(image: String)
        fun onItemClick(id: Long)
    }

}
package com.ar.mms.ui.articulos

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.ar.mms.R
import com.ar.mms.databinding.FragmentProductItemDialogBinding
import com.ar.mms.network.ImageRequester
import com.ar.mms.network.ProductEntry
import com.ar.mms.room.entity.ShoppingBasket
import com.ar.mms.room.viewmodel.ShoppingBasketViewModel
import com.ar.mms.ui.carrito.ShoppingCartFragment
import com.google.android.material.snackbar.Snackbar


class ProductDetailFragment private constructor() : Fragment() {

    private lateinit var myObject: ProductEntry
    private lateinit var mShoppingBasketViewModel: ShoppingBasketViewModel
    private val imageRequester: ImageRequester
    private var mQuantityDefault: Int
    private var mTotalyDefault: Double
    private lateinit var mBinding: FragmentProductItemDialogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            myObject = arguments!!.getSerializable(ARG_PARAM) as ProductEntry
        }
    }

    companion object {

        private val ARG_PARAM = "myFragment_caught"

        fun newInstance(caught: ProductEntry?): ProductDetailFragment {
            val args = Bundle()
            args.putSerializable(ARG_PARAM, caught)
            val fragment = ProductDetailFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mShoppingBasketViewModel = ViewModelProviders.of(this).get(ShoppingBasketViewModel::class.java)
        mBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_product_item_dialog,
            container,
            false
        )
        val view = mBinding.root
        Log.e("onDetails", "Value is: ${myObject}")
        imageRequester.setImageFromUrl(mBinding.productImageDetails, myObject.srcimg.toString())
        mBinding.productTitleDetails.setText(myObject.name)
        mBinding.productDescriptionDetails.setText(myObject.description)
        mBinding.productStockDetails.setText(myObject.stock.toString())
        mBinding.productPriceDetails.setText(myObject.amount.toString())
        mBinding.productControlQuantity.setText(mQuantityDefault.toString())
        mBinding.productControlAdd.setOnClickListener {

            mQuantityDefault++
            mTotalyDefault = myObject.amount!!.toDouble() * mQuantityDefault

            mBinding.productControlQuantity.setText(mQuantityDefault.toString())
            mBinding.productControlAmount.setText(mTotalyDefault.toString())
        }
        mBinding.productControlRemove.setOnClickListener {
            if (mQuantityDefault > 1) {
                mQuantityDefault--
                mTotalyDefault = myObject.amount!!.toDouble() * mQuantityDefault

                mBinding.productControlQuantity.setText(mQuantityDefault.toString())
                mBinding.productControlAmount.setText(mTotalyDefault.toString())
            }
        }


        mBinding.productControlAddBasket.setOnClickListener {
            val shoppingBasket = ShoppingBasket()
            shoppingBasket.amount = mTotalyDefault
            shoppingBasket.name = myObject.name
            shoppingBasket.quantity = mQuantityDefault
            shoppingBasket.sku = myObject.id.toString()
            shoppingBasket.additional = myObject.description
            mShoppingBasketViewModel.insertItem(shoppingBasket)
            view.let {
                Snackbar.make(it, " + " + shoppingBasket.name, Snackbar.LENGTH_SHORT)
                    .show()
            };
        }

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
        val nextFrag = ShoppingCartFragment()
    }

    init {
        imageRequester = ImageRequester
        mQuantityDefault = 0
        mTotalyDefault = 0.0
    }

    override fun onDestroy() {
        super.onDestroy()
        val nextFrag = ShoppingCartFragment()
        activity!!.supportFragmentManager.beginTransaction()
            .remove(nextFrag)
            .addToBackStack(null)
            .commit()
    }

}
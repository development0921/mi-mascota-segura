package com.ar.mms.ui.adoption

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ar.mms.R
import com.ar.mms.model.Adoption
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_item_adoption.view.*
import java.lang.IllegalArgumentException

class AdoptionAdapter(
    private val context: Context,
    private val eventsList: List<Adoption>,
    private val itemClickListener: OnClickListener
) : RecyclerView.Adapter<AdoptionAdapter.AdoptionViewHolder>() {

    override fun getItemCount(): Int = eventsList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdoptionViewHolder {
        return AdoptionViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.row_item_adoption, parent, false)
        )
    }

    override fun onBindViewHolder(holder: AdoptionViewHolder, position: Int) {
        when (holder) {
            is AdoptionViewHolder -> holder.bind(eventsList[position])
            else -> throw IllegalArgumentException("Se olvideo de pasar el Viewholder en el bind")  //fixme esto jamas pasara...
        }
    }

    inner class AdoptionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Adoption) {
            itemView.setOnClickListener { itemClickListener.onItemClick(item.id) }
          //  itemView.img_adoption_id.setOnClickListener { itemClickListener.onImageClick() }
            Glide.with(context).load(item.imagen).into(itemView.img_adoption_id)
            itemView.tv_name_adoption.text = item.name
            itemView.tv_descripcion_id.text = item.direccion
        }
    }

    interface OnClickListener {
        fun onImageClick(image: String)
        fun onItemClick(id: Long)
    }

}
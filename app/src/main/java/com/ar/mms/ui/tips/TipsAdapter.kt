package com.ar.mms.ui.tips

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ar.mms.R
import com.ar.mms.model.Tips
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_tips.view.*
import java.lang.IllegalArgumentException

class TipsAdapter(
    private val context: Context,
    private val eventsList: List<Tips>,
    private val itemClickListener: OnClickListener
) : RecyclerView.Adapter<TipsAdapter.TipsViewHolder>() {


    override fun getItemCount(): Int = eventsList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TipsViewHolder {
        return TipsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.row_tips, parent, false)
        )
    }

    override fun onBindViewHolder(holder: TipsViewHolder, position: Int) {
        when (holder) {
            is TipsViewHolder -> holder.bind(eventsList[position])
            else -> throw IllegalArgumentException("Se olvideo de pasar el Viewholder en el bind")  //fixme esto jamas pasara...
        }
    }

    inner class TipsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Tips) {
            itemView.setOnClickListener { itemClickListener.onItemClick(item.id) }
            //  itemView.img_adoption_id.setOnClickListener { itemClickListener.onImageClick() }
            Glide.with(context).load(item.imagen).into(itemView.img_veterinaria)
        }
    }

    interface OnClickListener {
        fun onImageClick(image: String)
        fun onItemClick(id: Long)
    }

}
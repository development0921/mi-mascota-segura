package com.ar.mms.ui.tips

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ar.mms.databinding.FragmentTipsDetailsBinding
import com.ar.mms.model.Tips
import com.bumptech.glide.Glide

class TipsDetailsFragment : Fragment() {

    private lateinit var binding: FragmentTipsDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentTipsDetailsBinding.inflate(inflater)

        if (arguments != null) {
            val tips: Tips? = TipsDetailsFragmentArgs.fromBundle(requireArguments()).tipsSelectedProperty

            binding.tvNameTips.text = tips?.titulo
            binding.descriptionId.text = tips?.descricion


            Glide.with(this).load(tips?.imagen).into(binding.imgAdoptionId)
        }

        return binding.root
    }

}
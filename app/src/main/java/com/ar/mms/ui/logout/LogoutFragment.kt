package com.ar.mms.ui.logout

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ar.mms.R
import com.ar.mms.activities.LoginActivity

class LogoutFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        startActivity(Intent(context, LoginActivity::class.java))

        val mview = inflater.inflate(R.layout.fragment_logout, container, false)
        return mview
    }


}

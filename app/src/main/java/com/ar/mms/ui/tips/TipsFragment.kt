package com.ar.mms.ui.tips

import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.util.Log
import android.view.View
import com.ar.mms.R
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ar.mms.model.Tips
import com.google.firebase.database.FirebaseDatabase
import java.util.HashMap

/**
 * A simple [Fragment] subclass.
 */
class TipsFragment : Fragment(), TipsAdapter.OnClickListener {

    private lateinit var recyclerView: RecyclerView
    private val mDatabase = FirebaseDatabase.getInstance().reference
    private var mTipsList = mutableListOf<Tips>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_tips, container, false)
        setUpView(view)
        return view
    }

    private fun setUpView(view: View) {
        mTipsList = emptyList<Tips>().toMutableList()
        recyclerView = view.findViewById(R.id.recycler_tips)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {

        recyclerView.layoutManager = GridLayoutManager(activity, 2)
//        recyclerView.addItemDecoration(
//            DividerItemDecoration(
//                activity,
//                DividerItemDecoration.VERTICAL
//            )
//        )

        geTipsFromFirebaseAndUpdateRecyclerView()

    }

    fun geTipsFromFirebaseAndUpdateRecyclerView() {

        mDatabase.child("Tips").get().addOnSuccessListener { dataSnapshot ->
            Log.i("firebase Tips", "Got value ${dataSnapshot.value}")

            for (postSnapshot in dataSnapshot.children) {
                val tipsFirebase = (postSnapshot.value) as HashMap<String, String>
                val tips = Tips(
                    id = tipsFirebase["id"] as Long,
                    titulo = tipsFirebase["titulo"].toString(),
                    imagen = tipsFirebase["imagen"].toString(),
                    descricion = tipsFirebase["descripcion"].toString()
                )
                mTipsList.add(tips)
            }

            showRecyclerView()

        }.addOnFailureListener {
            Log.e("firebase tips", "Error getting data", it)
        }
    }

    private fun showRecyclerView() {
        if (mTipsList.isNotEmpty())
            recyclerView.adapter = activity?.applicationContext?.let {
                TipsAdapter(it, mTipsList, this)
            }
    }

    override fun onImageClick(image: String) {
        TODO("Not yet implemented")
    }

    override fun onItemClick(id: Long) {
        val action =
            TipsFragmentDirections.actionTipsFragmentToTipsDetailsFragment(mTipsList.first { it.id == id })
        findNavController()?.navigate(action)
    }

}
package  com.ar.mms.application

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.google.firebase.FirebaseApp
import com.google.firebase.database.FirebaseDatabase

class MainApplication : Application() {

    companion object {
        lateinit var instance: MainApplication
            private set

    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        FirebaseApp.initializeApp(this)
        FirebaseDatabase.getInstance().setPersistenceEnabled(true)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

}
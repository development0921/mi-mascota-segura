package com.ar.mms.network

import android.net.Uri
import java.io.Serializable

/**
 * A product entry in the list of products.
 */

class ProductEntry : Serializable{

    var amount: Long? = 0
    var id: Long? = 0
    var name: String? = ""
    var description: String? = ""
    var sale: String? = ""
    var srcimg: String? = ""
    var stock: Long? = 0
    val dynamicUrl: Uri = Uri.parse(srcimg)
    override fun toString(): String {
        return "ProductEntry(amount=$amount, id=$id, name=$name, description=$description, sale=$sale, srcimg=$srcimg, stock=$stock, dynamicUrl=$dynamicUrl)"
    }


}
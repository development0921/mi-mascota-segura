package com.ar.mms.adapter;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ar.mms.R;
import com.ar.mms.fragments.nutricionDelivery.QuestionFiveFragment;
import com.ar.mms.fragments.nutricionDelivery.QuestionFourFragment;
import com.ar.mms.fragments.nutricionDelivery.QuestionOneFragment;
import com.ar.mms.fragments.nutricionDelivery.QuestionSixFragment;
import com.ar.mms.fragments.nutricionDelivery.QuestionThreeFragment;
import com.ar.mms.fragments.nutricionDelivery.QuestionTwoFragment;

public class NutricionDeliveryAdapter extends FragmentPagerAdapter {

    private String[] mTitles;
    private Context mCtx;

    public NutricionDeliveryAdapter(FragmentManager fm, Context ctx) {
        super(fm);
        mCtx = ctx;
        mTitles = ctx.getResources().getStringArray(R.array.setup_tabs);
    }

    @Override
    public Fragment getItem(int position) {
        return loadViewByPosition(position);
    }

    @Override
    public int getCount() {
        return mTitles.length;
    }

    private Fragment loadViewByPosition(int position) {
        Fragment questionFragment = null;
        switch (position) {
            default:
            case 0:
                questionFragment = QuestionOneFragment.newInstance();
                return questionFragment;
            case 1:
                questionFragment = QuestionTwoFragment.newInstance();
                return questionFragment;
            case 2:
                questionFragment = QuestionThreeFragment.newInstance();
                return questionFragment;
            case 3:
                questionFragment = QuestionFourFragment.newInstance();
                return questionFragment;
            case 4:
                questionFragment = QuestionFiveFragment.newInstance();
                return questionFragment;
            case 5:
                questionFragment = QuestionSixFragment.newInstance();
                return questionFragment;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int postion) {
        return mTitles[postion].toString();
    }
}


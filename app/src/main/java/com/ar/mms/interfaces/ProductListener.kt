package com.ar.mms.interfaces

import com.ar.mms.network.ProductEntry

interface ProductListener {
    fun onDetails(productEntry: ProductEntry?)
}
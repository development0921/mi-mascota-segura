package com.ar.mms.interfaces;

public interface ResponsePostListener {

    void onResponse(String response);
    void cancel(String msj);
}

package com.ar.mms.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Products(
    val id: Long = 0,
    val amount: String = "",
    val description: String = "",
    val name: String = "",
    val sale: String = "",
    val srcimg: String = "",
    val stock: String = ""
) : Parcelable


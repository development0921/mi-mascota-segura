package com.ar.mms.model

import android.os.Parcelable
import com.google.firebase.database.IgnoreExtraProperties
import kotlinx.android.parcel.Parcelize

@IgnoreExtraProperties
@Parcelize
data class Pet(
    val avatar: String = "",
    val birth: String = "",
    val clinicHistory: String = "",
    val code: String = "",
    val gender: String = "",
    val lastCheck: String = "",
    val name: String = "",
    val owner: String = "",
    val race: String = "",
    val type: String = "",
    val weight: String = ""
) : Parcelable




package com.ar.mms.model

import android.os.Parcelable
import com.google.firebase.database.IgnoreExtraProperties
import kotlinx.android.parcel.Parcelize

@IgnoreExtraProperties
@Parcelize
data class Owner(
    val DireccionVeteAfiliada: String? = "",
    val TelefonoVeteAfeiliada: String? = "",
    val address: String? = "",
    val birthday: String? = "",
    val city: String? = "",
    val country: String? = "",
    val dni: String? = "",
    val email: String? = "",
    val firstName: String? = "",
    val idPets: String? = "",
    val lastName: String? = "",
    val manager: String? = "",
    val nacionality: String? = "",
    val petQty: String? = "",
    val phone: String? = "",
    val status: String? = "",
    val user: String? = ""
) : Parcelable
package com.ar.mms.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Veterinaria(
    val id: Long = 0,
    val name: String = "",
    val imagen: String = "",
    val contacto: String = "",
    val direccion: String = "",
    val gps: String = "",
    val description: String = ""
) : Parcelable
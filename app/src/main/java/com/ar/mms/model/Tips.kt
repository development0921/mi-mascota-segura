package com.ar.mms.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Tips(
    val id: Long = 0,
    val imagen:String = "",
    val titulo:String = "",
    val descricion:String = ""
) : Parcelable

package com.ar.mms.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PetLost(
    val id: Long = 0,
    val contacto: String = "",
    val direccion:String = "",
    val fecha:String = "",
    val imagen:String = "",
    val name:String = ""
) : Parcelable



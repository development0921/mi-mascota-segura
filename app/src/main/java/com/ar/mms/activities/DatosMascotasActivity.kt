package com.ar.mms.activities


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import com.ar.mms.R
import com.ar.mms.model.Pet
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.ar.mms.model.Owner
import com.bumptech.glide.Glide

class DatosMascotasActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_datos_mascotas_activity)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        //  toolbar.setTitle("Tu mascotas");
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true);

        if (intent != null && intent.extras != null) {
            val petInfo: Pet? = intent.extras?.getParcelable("petInfo")
            val ownerInfo: Owner? = intent.extras?.getParcelable("ownerInfo")
            setUpView(petInfo, ownerInfo)
        }


    }

    private fun setUpView(pet: Pet?, owner: Owner?) {
        val tvDni = findViewById<TextView>(R.id.info_dni_id)
        tvDni.text = owner!!.dni
        val tvNombre = findViewById<TextView>(R.id.info_nombre_id)
        tvNombre.text = pet!!.name
        val tvRaza = findViewById<TextView>(R.id.info_raza_id)
        tvRaza.text = pet.race
        val tvSexo = findViewById<TextView>(R.id.info_sexo_id)
        tvSexo.text = pet.gender
        val tvEdad = findViewById<TextView>(R.id.info_edad_id)
        tvEdad.text = pet.birth //fixme edad????
        val tvPeso = findViewById<TextView>(R.id.info_peso_id)
        tvPeso.text = pet.weight
        val tvHistoria = findViewById<TextView>(R.id.info_historia_id)
        tvHistoria.text = pet.clinicHistory
        val tvChequeo = findViewById<TextView>(R.id.info_chequeo_id)
        tvChequeo.text = pet.lastCheck
        val tvOwner = findViewById<TextView>(R.id.info_nombre_owner_id)
        tvOwner.text = owner.firstName + " " + owner.lastName
        val tvCorreo = findViewById<TextView>(R.id.info_correo_owner_id)
        tvCorreo.text = owner.email
        val tvTelefono = findViewById<TextView>(R.id.info_telefono_owner_id)
        tvTelefono.text = owner.phone
        val tvDomicilio = findViewById<TextView>(R.id.info_domicilio_owner_id)
        tvDomicilio.text = owner.address

        val tvDireccionVete = findViewById<TextView>(R.id.info_veterianaria_domicilio_id)
        tvDireccionVete.text = owner.DireccionVeteAfiliada
        val tvTelefonoVeterinaria = findViewById<TextView>(R.id.info_veterinaria_telefono_id)
        tvTelefonoVeterinaria.text = owner.TelefonoVeteAfeiliada

        loadImage(pet.avatar)
    }

    private fun loadImage(image: String) {
        val imageView = findViewById<ImageView>(R.id.img_mascotas_id)
        Glide.with(this)
            .load(image)
            .placeholder(R.drawable.img_dog)
            .into(imageView)
    }
}
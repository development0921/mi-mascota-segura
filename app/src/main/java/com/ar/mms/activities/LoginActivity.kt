package com.ar.mms.activities

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import com.ar.mms.R
import android.widget.EditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import android.os.Bundle
import butterknife.ButterKnife
import com.google.firebase.FirebaseApp
import android.content.Intent
import android.content.Context
import android.widget.Toast
import butterknife.OnClick
import android.os.Build
import androidx.core.app.ActivityCompat
import android.content.pm.PackageManager
import android.widget.Button
import com.ar.mms.application.MainApplication

class LoginActivity : AppCompatActivity() {

    private var mFirebaseAuth: FirebaseAuth? = null
    private var mAuthStateListener: AuthStateListener? = null
    private val mPermissions = arrayOf(
        Manifest.permission.INTERNET,
        Manifest.permission.ACCESS_WIFI_STATE,
        Manifest.permission.ACCESS_NETWORK_STATE,
        Manifest.permission.CHANGE_WIFI_STATE,  /*   android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                       android.Manifest.permission.READ_EXTERNAL_STORAGE,
                       android.Manifest.permission.WAKE_LOCK,
                       android.Manifest.permission.EXPAND_STATUS_BAR,
                       android.Manifest.permission.READ_LOGS,
                       android.Manifest.permission.BATTERY_STATS,
                       android.Manifest.permission.READ_PHONE_STATE,
                       android.Manifest.permission.ACCESS_FINE_LOCATION,
                       android.Manifest.permission.ACCESS_COARSE_LOCATION,
                       android.Manifest.permission.RECEIVE_BOOT_COMPLETED,
                       android.Manifest.permission.CAMERA,
                       android.Manifest.permission.READ_CONTACTS,
                       android.Manifest.permission.WRITE_CONTACTS,
                       android.Manifest.permission.GET_ACCOUNTS,*/
        Manifest.permission.ACCESS_WIFI_STATE
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        ButterKnife.bind(this)
        FirebaseApp.initializeApp(this)
        mFirebaseAuth = FirebaseAuth.getInstance()
        startActivity(Intent(this@LoginActivity, MainActivity::class.java))
        mAuthStateListener = AuthStateListener { firebaseAuth ->
            val user = firebaseAuth.currentUser
            //login success
            if (user != null) {
                startActivity(Intent(this@LoginActivity, MainActivity::class.java))
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTH_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "welcome", Toast.LENGTH_SHORT).show()
                startActivity(Intent(this@LoginActivity, MainActivity::class.java))
            } else {
                Toast.makeText(this, "Ingrese sus datos para continuar", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mFirebaseAuth!!.addAuthStateListener(mAuthStateListener!!)
    }

    override fun onPause() {
        super.onPause()
        if (mAuthStateListener != null) {
            mFirebaseAuth!!.removeAuthStateListener(mAuthStateListener!!)
        }
    }


    private fun checkForPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!hasPermissions(this)) {
                ActivityCompat.requestPermissions(this, mPermissions, PERMISSION_ALL)
            }
        }
    }

    private fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
        if (context != null && permissions != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }

    companion object {
        const val AUTH_REQUEST_CODE = 9321
        const val UNKNOW_PROVIDER = "UNKNOW PROVIDER"
        private const val PERMISSION_ALL = 1
    }
}
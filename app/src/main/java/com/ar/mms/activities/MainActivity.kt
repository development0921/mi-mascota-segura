package com.ar.mms.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.ui.AppBarConfiguration
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.Toolbar
import com.ar.mms.R
import com.google.firebase.FirebaseApp
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import com.google.android.material.navigation.NavigationView
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.ar.mms.helpers.MmsSharedPreferences
import com.ar.mms.ui.auth.AuthActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse

class MainActivity : AppCompatActivity() {

//    private val resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
//        val response = IdpResponse.fromResultIntent(it.data)
//
//        if (it.resultCode == RESULT_OK){
//            val user = FirebaseAuth.getInstance().currentUser
//            if (user != null){
//                Toast.makeText(this,"Bienvenido",Toast.LENGTH_SHORT).show()
//            }
//        } else {
//            if (response == null){
//                Toast.makeText(this,"Hasta luego",Toast.LENGTH_SHORT).show()
//                finish()
//            }
//        }
//    }


    private var mAppBarConfiguration: AppBarConfiguration? = null
    private lateinit var mFirebaseAuth: FirebaseAuth
    private lateinit var navController: NavController
    private lateinit var mAuthStateListener: FirebaseAuth.AuthStateListener

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        //cerrar



        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        FirebaseApp.initializeApp(this)
        MmsSharedPreferences.instance?.initSharedPreferencesManager(this)
        mFirebaseAuth = FirebaseAuth.getInstance()
//        mAuthStateListener = FirebaseAuth.AuthStateListener { auth ->
//            if (auth.currentUser != null){
//                //se oculta el progressbar
//            } else {
//                val providers = arrayListOf(
//                    AuthUI.IdpConfig.EmailBuilder().build(),
//                    AuthUI.IdpConfig.GoogleBuilder().build())
//
//                resultLauncher.launch(
//                    AuthUI.getInstance()
//                        .createSignInIntentBuilder()
//                        .setAvailableProviders(providers)
//                        .build())
//            }
//        }
//



        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val navigationView = findViewById<NavigationView>(R.id.nav_view)

        mAppBarConfiguration = AppBarConfiguration.Builder(
            R.id.nav_home_id,
            R.id.nav_veterinaria_id,
            R.id.nav_events_id,
            R.id.nav_salud_id,
            R.id.nav_tips_id,
            R.id.nav_articulos_id,
            R.id.nav_adoption_id,
            R.id.nav_logout_id,
            R.id.nav_shopping_cart_id,
            R.id.nav_contacto_id,
            R.id.nav_pet_lost_id
        ).setDrawerLayout(drawer)
            .build()
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration!!)
        NavigationUI.setupWithNavController(navigationView, navController)


        // navController.popBackStack(R.id.nav_home_id, true);
    }



    override fun onSupportNavigateUp(): Boolean {
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        return NavigationUI.navigateUp(
            navController,
            mAppBarConfiguration!!
        ) || super.onSupportNavigateUp()
    }



    override fun onBackPressed() {
        super.onBackPressed()
        navController.navigate(R.id.nav_home_id)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.editDatosMascotas -> {
                startActivity(Intent(this, DatosMascotaEditActivity::class.java))
                return true
            }
            R.id.signUpButton -> {
                AuthUI.getInstance().signOut(this)
                    .addOnSuccessListener {
                        Toast.makeText(this,"Hasta luego",Toast.LENGTH_SHORT).show()
                        MmsSharedPreferences.instance?.restoreSession()
                        startActivity(Intent(this, AuthActivity::class.java))
                    }
            }

        }
        return super.onOptionsItemSelected(item)
    }

//    override fun onResume() {
//        super.onResume()
//        mFirebaseAuth.addAuthStateListener(mAuthStateListener)
//    }
//
//    override fun onPause() {
//        super.onPause()
//        mFirebaseAuth.removeAuthStateListener(mAuthStateListener)
//    }
//

}
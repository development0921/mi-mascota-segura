package com.ar.mms

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.lang.Exception

class MessagingService : FirebaseMessagingService() {
    val TAG = "MessagingService"
    override fun onMessageReceived(p0: RemoteMessage) {

    }

    override fun onMessageSent(p0: String) {

    }

    override fun onDeletedMessages() {

    }

    override fun onSendError(p0: String, p1: Exception) {

    }

    override fun onNewToken(p0: String) {

    }
}

package com.ar.mms.Helper;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FontManager {

    public static final String ROOT = "fonts/",
            FONTAWESOME = ROOT + "fontawesome-webfont.ttf";

    /**
     *  font awesome fonts...
     * @param context
     * @param font
     * @return
     */
    public static Typeface getTypeface(Context context, String font) {
        return Typeface.createFromAsset(context.getAssets(), font);
    }

    /**
     * usar otra fuentes...
     * yourTextView.setTypeface(FontManager.getTypeface(FontManager.YOURFONT));
     */

    /**
     *
     * font awesome icons
     */

}
package com.ar.mms.fragments.nutricionDelivery;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.ar.mms.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionTwoFragment extends Fragment {


    public QuestionTwoFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance() {
        return new QuestionTwoFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_question_two, container, false);
    }

}

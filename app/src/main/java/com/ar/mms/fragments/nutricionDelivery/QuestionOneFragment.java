package com.ar.mms.fragments.nutricionDelivery;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ar.mms.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionOneFragment extends Fragment {


    //linearIsCar
    //linearIsCar

    public QuestionOneFragment() {
        // Required empty public constructor
    }

    public static QuestionOneFragment newInstance() {
        return new QuestionOneFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_question_one, container, false);
    }

}

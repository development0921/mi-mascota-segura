package com.ar.mms.room.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "shopping_basket")
public class ShoppingBasket implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "sku")
    private String sku;
    @ColumnInfo(name = "quantity")
    private Integer quantity;
    @ColumnInfo(name = "amount")
    private Double amount;
    @ColumnInfo(name = "date")
    private String date;
    @ColumnInfo(name = "additional")
    private String additional;

    public ShoppingBasket() {
    }

    @Ignore
    public ShoppingBasket(Integer id, String name, String sku, Integer quantity, Double amount, String date, String additional) {
        this.id = id;
        this.name = name;
        this.sku = sku;
        this.quantity = quantity;
        this.amount = amount;
        this.date = date;
        this.additional = additional;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }
}

package com.ar.mms.room.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.ar.mms.room.entity.ShoppingBasket;
import com.ar.mms.room.repository.ShoppinBasketRepository;

import java.util.List;


public class ShoppingBasketViewModel extends AndroidViewModel {

    private ShoppinBasketRepository mRepository;
    private LiveData<Integer> mItemsCount;

    public ShoppingBasketViewModel(@NonNull Application application) {
        super(application);
        mRepository = new ShoppinBasketRepository(application);
        mItemsCount = mRepository.getItemsCount();
    }


    public void insertItem(ShoppingBasket beneficiary) {
        mRepository.insertItemToBasket(beneficiary);
    }

    public void updateItem(ShoppingBasket beneficiary) {
        mRepository.updateItemToBasket(beneficiary);
    }

    public LiveData<Integer> getItemsCount()
    {
        return mItemsCount;
    }

    public LiveData<List<ShoppingBasket>> getValidationPagination(int limitNumber, int offsetNumber) {
        return mRepository.getPagination(limitNumber,offsetNumber);
    }
}

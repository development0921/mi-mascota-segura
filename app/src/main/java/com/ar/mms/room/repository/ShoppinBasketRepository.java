package com.ar.mms.room.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.ar.mms.room.dao.ShoppingBasketDao;
import com.ar.mms.room.db.AppDatabase;
import com.ar.mms.room.entity.ShoppingBasket;

import java.util.List;

public class ShoppinBasketRepository {

    private ShoppingBasketDao shoppingBasketDao;
    private LiveData<Integer> integerLiveData;

    public ShoppinBasketRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        shoppingBasketDao = db.mShoppingBasketDao();
        integerLiveData = shoppingBasketDao.getItemsCount();
    }

    public ShoppinBasketRepository(AppDatabase db) {
        shoppingBasketDao = db.mShoppingBasketDao();
        integerLiveData = shoppingBasketDao.getItemsCount();
    }


    public LiveData<Integer> getItemsCount() {
        return integerLiveData;
    }

    public void insertItemToBasket(ShoppingBasket beneficiary) {
        new InsertItemAsyncTask(shoppingBasketDao).execute(beneficiary);
    }

    public LiveData<List<ShoppingBasket>> getPagination(int limitNumber, int offsetNumber) {
        return shoppingBasketDao.getPagination(limitNumber,offsetNumber);
    }

    public class InsertItemAsyncTask extends AsyncTask<ShoppingBasket, Void, Void> {

        private ShoppingBasketDao mAsyncTaskDao;

        public InsertItemAsyncTask(ShoppingBasketDao orderTicketDao) {
            mAsyncTaskDao = orderTicketDao;
        }

        @Override
        protected Void doInBackground(final ShoppingBasket... params) {
            mAsyncTaskDao.insertInBasket(params[0]);
            return null;
        }

    }

    public void updateItemToBasket(ShoppingBasket beneficiary) {
        new updateItemAsyncTask(shoppingBasketDao).execute(beneficiary);
    }

    public class updateItemAsyncTask extends AsyncTask<ShoppingBasket, Void, Void> {

        private ShoppingBasketDao mAsyncTaskDao;

        public updateItemAsyncTask(ShoppingBasketDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final ShoppingBasket... params) {
            mAsyncTaskDao.updateItem(params[0]);
            return null;
        }
    }

}

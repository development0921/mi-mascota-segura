package com.ar.mms.room.db;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.ar.mms.room.dao.ShoppingBasketDao;
import com.ar.mms.room.entity.ShoppingBasket;


@Database(entities = {ShoppingBasket.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "mmsdb";
    private static AppDatabase INSTANCE;

    public abstract ShoppingBasketDao mShoppingBasketDao();


    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME)
//                            .addMigrations(MIGRATION_1_2)
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }


    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {

        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            // TODO: 01/05/2020 async work to populate
        }
    };


    /**
     * Migrate from:
     * version 1
     * to
     * version 2
     */
    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE Beneficiaries "
                    + " ADD COLUMN HadPreviousBenefit INTEGER NOT NULL DEFAULT 0");

            database.execSQL("CREATE TABLE IF NOT EXISTS  `Validation` "
                    + "(`Id` INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "`DocumentType` TEXT, "
                    + "`DocumentNumber` TEXT, "
                    + "`FullName` TEXT, "
                    + "`LastName` TEXT, "
                    + "`Date` TEXT, "
                    + "`Bag` TEXT, "
                    + "`Quantity` INTEGER NOT NULL DEFAULT 0, "
                    + "`PointOfDelivery` TEXT)");
        }
    };


}
package com.ar.mms.room.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.ar.mms.room.entity.ShoppingBasket;

import java.util.List;

@Dao
public interface  ShoppingBasketDao {


    @Query("SELECT COUNT(*) FROM shopping_basket")
    LiveData<Integer> getItemsCount();

    @Insert
    void insertInBasket(ShoppingBasket product);

    @Update
    void updateItem(ShoppingBasket... product);

    @Query("SELECT * FROM shopping_basket LIMIT :limitNumber OFFSET :offsetNumber")
    LiveData<List<ShoppingBasket>> getPagination(int limitNumber, int offsetNumber);

//    @Query("UPDATE  shopping_basket SET  Sync = 'S' WHERE Id = :beneficiaryId")
//    void updateItemById(Integer itemId);
}

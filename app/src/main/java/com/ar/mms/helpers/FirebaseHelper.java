package com.ar.mms.helpers;

public class FirebaseHelper {
    private static final FirebaseHelper ourInstance = new FirebaseHelper();
    private static final String ROOT = "mimascotasegura-ar";
    private static final String BRANCH_PRODUCTS = "products";

    static FirebaseHelper getInstance() {
        return ourInstance;
    }

    private FirebaseHelper() {
    }

    public static String getBranchProducts() {
        return BRANCH_PRODUCTS;
    }

    public static String getROOT() {
        return ROOT;
    }
}

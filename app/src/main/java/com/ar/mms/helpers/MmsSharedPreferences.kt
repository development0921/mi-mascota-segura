package com.ar.mms.helpers

import android.content.Context
import android.content.SharedPreferences

class MmsSharedPreferences private constructor(context: Context)  {
    private val sharedPreferences: SharedPreferences
    private var isLogged: Boolean = false

    companion object{
         const val SHARED_PREFERENCES_PACKAGE = "com.ar.mms"

        var instance: MmsSharedPreferences? = null
            private set
    }


    init {
        sharedPreferences =
            context.getSharedPreferences(MmsSharedPreferences.SHARED_PREFERENCES_PACKAGE, Context.MODE_PRIVATE)
    }

    fun initSharedPreferencesManager(context: Context): MmsSharedPreferences? {
        if (instance == null) {
            instance = MmsSharedPreferences(context)
        }
        return instance
    }



    var inSession: Boolean
        get() = sharedPreferences.getBoolean("IN_SESSION", false
        )
        set(value) {
            sharedPreferences.edit().putBoolean("IN_SESSION", value
            ).apply()
        }

    fun saveSession(value : Boolean){
        inSession = value
    }

    fun restoreSession(){
        sharedPreferences.edit().clear().apply()
    }


}
